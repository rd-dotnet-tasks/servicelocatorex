﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceLocator
{
    interface IServiceLocator
    {
        T getService<T>();
        T getService<T>(String serviceName);
    }

    interface IServiceA { };
    class ServiceA : IServiceA { };
    interface IServiceB { };
    class ServiceB : IServiceB { };


    class ServiceLocator : IServiceLocator
    {
        IDictionary<object, object> _services;
        public static readonly ServiceLocator locator = new ServiceLocator();
        private ServiceLocator()
        {
            _services = new Dictionary<object, object>();
            Console.WriteLine(typeof(IServiceA));
            _services.Add(typeof(IServiceA), new ServiceA());
            _services.Add(typeof(IServiceB), new ServiceB());
        }
        public T getService<T>()
        {
            return (T)_services[typeof(T)];
        }

        public T getService<T>(string serviceName)
        {
            throw new NotImplementedException();
        }
    }
}
