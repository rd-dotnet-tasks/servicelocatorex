﻿using System;

namespace ServiceLocator
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceLocator serviceLocator = ServiceLocator.locator;
            Console.WriteLine(serviceLocator.getService<IServiceA>());
            Console.WriteLine(serviceLocator.getService<IServiceB>());
            Console.ReadKey();
        }
    }
}
